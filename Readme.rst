Trabajo 3 
=========

.. figure:: http://imageshack.com/a/img854/2245/c9a7.png

**Fecha** : 2/13/14

**Autor** : Jes�s Hern�ndez Garc�a

*INTRODUCCI�N*

Mi trabajo consiste en una p�gina web en html , con dos hojas de estilo css para peque�as animaciones .
La p�gina web va enfocada a el mundo del motor como tema principal . En concreto la he hecho para NUEVE80RACING , un grupo que tenemos en zamora de varios aficionados a este mundo .

*CONTENIDO*

La p�gina web esta formada por 10 p�ginas , explicando un poco lo que es el mundo del motor y sus diferentes disciplinas . Una p�gina es de nuestros v�deos donde mostramos un poco nuestras habilidades de conducci�n. Tenemos tambi�n una de entretenimiento , donde he insertado dos juegos flash para hacer un poco m�s entretenido nuestros momentos de relax y tambi�n un formulario para poder contactartar con nosotros e indicarnos dudas y propuestas . Pocas im�genes est�n sacadas de internet , casi todas son de nuestro club .

*Conclusi�n*

Para m� es una p�gina sencilla pero muy intuitiva , se puede abrir en todos los navegadores y est� adaptada a varias resoluciones para que no tengamos el problema de las redimensiones . La p�gina al estar hecha desde c�digo , visualmente es sencilla , ya que actualmente desde c�digo para  hacer, por ejemplo , solo un men� desplegable, nos ocupar�a mas de 30 l�nes y es pr�cticamente imposible saber todas las etiquetas que esto conlleva.Con el tiempo que hemos tenido los resultados han sido bastante sastisfactorios , aunque se podr�a mejoras bastante si usamos un entorno gr�fico para crear la web.


Jes�s Hern�ndez Garc�a
